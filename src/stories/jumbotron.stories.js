import Jumbotron from '../components/Jumbotron.vue';

export default {
  title: 'Jumbotron',
  component: {Jumbotron},
  argTypes: {
    label: 'String',
    backgroundColor: { control: 'color' },
  },
};


export const Vertical = {
    parameters: {
        layout: 'centered',
        design: {
            type: 'figma',
            url: 'https://www.figma.com/file/T3Lagt1IIb4LXo48yZOGPp/Collaboration-Channels-High-Fi?type=design&node-id=1348%3A1953&t=BPa3wUOEpsFNm6u2-1'
        }
    },
    args: { 
        title: 'My Channels',
        subtitle: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Proin libero nunc consequat interdum. Curabitur gravida arcu ac tortor dignissim convallis aenean.',
    },
    render: (args) => ({
        components: { Jumbotron },
        setup() {
            return { args };
        },
        template: '<Jumbotron v-bind="args" />',
    }),
};

export const Horizontal = {
    parameters: {
        layout: 'padded',
        design: {
            type: 'figma',
            url: 'https://www.figma.com/file/T3Lagt1IIb4LXo48yZOGPp/Collaboration-Channels-High-Fi?type=design&node-id=1348%3A1957&t=BPa3wUOEpsFNm6u2-1'
        }
    },
    args: {
        ...Vertical.args,
        title: 'ObjectiveDask01',
        subtitle: 'Owner: Regulaition',
        horizontal: true,
    },
    render: (args) => ({
        components: { Jumbotron },
        setup() {
            return { args };
        },
        template: '<Jumbotron v-bind="args" />',
    }),
};

export const fullLength = {
    parameters: {
        layout: 'padded',
        design: {
            type: 'figma',
            url: 'https://www.figma.com/file/T3Lagt1IIb4LXo48yZOGPp/Collaboration-Channels-High-Fi?type=design&node-id=1378%3A1973&t=alSF1RPM0ReKXdcO-1'
        }
    },
    args: {
        ...Vertical.args,
        title: 'Project Idris',
        subtitle: 'Understand our risk exposure to interest rate increases across all our investment portfolio & design a new investment strategy.',
        fullLength: true,
    },
    render: (args) => ({
        components: { Jumbotron },
        setup() {
            return { args };
        },
        template: '<Jumbotron v-bind="args" />',
    }),
};