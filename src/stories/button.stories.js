import Button from '../components/Button.vue';

export default {
  title: 'Button',
  component: {Button},
  argTypes: {
    label: 'String',
    backgroundColor: { control: 'color' },
  },
};


export const Primary = {
    render: (args) => ({
        components: { Button },
        setup() {
            return { args };
        },
        template: '<Button v-bind="args" />',
    }),
};

export const Secondary = {
    args: {
        ...Primary.args,
        label: '😄👍😍💯',
    },
    render: (args) => ({
        components: { Button },
        setup() {
            return { args };
        },
        template: '<Button v-bind="args" />',
    }),
};
